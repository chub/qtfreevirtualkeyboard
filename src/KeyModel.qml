import QtQuick 2.0

/**
 * This is quick and dirty model for the keys of the InputPanel *
 * The code has been derived from
 * http://tolszak-dev.blogspot.de/2013/04/qplatforminputcontext-and-virtual.html
 * Copyright 2015 Uwe Kindler
 * Licensed under MIT see LICENSE.MIT in project root
 */
Item {
    property QtObject firstRowModel: first
    property QtObject secondRowModel: second
    property QtObject thirdRowModel: third

    ListModel {
        id:first
        ListElement { letter: "a"; keycode: Qt.Key_A;       firstSymbol: "²"; symbolKeycode: Qt.Key_twosuperior;   accent: "à"; accentKeycode: Qt.Key_Aacute }
        ListElement { letter: "z"; keycode: Qt.Key_Z;       firstSymbol: "1"; symbolKeycode: Qt.Key_1 ;            accent: "â"; accentKeycode: Qt.Key_Acircumflex }
        ListElement { letter: "e"; keycode: Qt.Key_E;       firstSymbol: "2"; symbolKeycode: Qt.Key_2;             accent: "á"; accentKeycode: Qt.Key_Agrave }
        ListElement { letter: "r"; keycode: Qt.Key_R;       firstSymbol: "3"; symbolKeycode: Qt.Key_3;             accent: "ä"; accentKeycode: Qt.Key_Adiaeresis }
        ListElement { letter: "t"; keycode: Qt.Key_T;       firstSymbol: "4"; symbolKeycode: Qt.Key_4;             accent: "ã"; accentKeycode: Qt.Key_Atilde }
        ListElement { letter: "y"; keycode: Qt.Key_Y;       firstSymbol: "5"; symbolKeycode: Qt.Key_5;             accent: "å"; accentKeycode: Qt.Key_Aring }
        ListElement { letter: "u"; keycode: Qt.Key_U;       firstSymbol: "6"; symbolKeycode: Qt.Key_6;             accent: "ç"; accentKeycode: Qt.Key_Ccedilla}
        ListElement { letter: "i"; keycode: Qt.Key_I;       firstSymbol: "7"; symbolKeycode: Qt.Key_7;             accent: "§"; accentKeycode: Qt.Key_section }
        ListElement { letter: "o"; keycode: Qt.Key_O;       firstSymbol: "8"; symbolKeycode: Qt.Key_8;             accent: "^"; accentKeycode: Qt.Key_AsciiCircum }
        ListElement { letter: "p"; keycode: Qt.Key_P;       firstSymbol: "9"; symbolKeycode: Qt.Key_9;             accent: "¨"; accentKeycode: Qt.Key_diaeresis  }
	ListElement { letter: "+"; keycode: Qt.Key_P;       firstSymbol: "0"; symbolKeycode: Qt.Key_0;             accent: "°"; accentKeycode: Qt.Key_degree  }
    }
    ListModel {
        id:second
        ListElement { letter: "q"; keycode: Qt.Key_Q;       firstSymbol: "!"; symbolKeycode: Qt.Key_Exclam;        accent: "è"; accentKeycode: Qt.Key_Egrave  }
        ListElement { letter: "s"; keycode: Qt.Key_S;       firstSymbol: "="; symbolKeycode: Qt.Key_Equal;         accent: "ê"; accentKeycode: Qt.Key_Ecircumflex  }
        ListElement { letter: "d"; keycode: Qt.Key_D;       firstSymbol: "#"; symbolKeycode: Qt.Key_NumberSign;    accent: "é"; accentKeycode: Qt.Key_Eacute  }
        ListElement { letter: "f"; keycode: Qt.Key_F;       firstSymbol: "$"; symbolKeycode: Qt.Key_Dollar;        accent: "ë"; accentKeycode: Qt.Key_Ediaeresis  }
        ListElement { letter: "g"; keycode: Qt.Key_G;       firstSymbol: "%"; symbolKeycode: Qt.Key_Percent;       accent: "ì"; accentKeycode: Qt.Key_Igrave  }
        ListElement { letter: "h"; keycode: Qt.Key_H;       firstSymbol: "&"; symbolKeycode: Qt.Key_Ampersand;     accent: "î"; accentKeycode: Qt.Key_Icircumflex  }
        ListElement { letter: "j"; keycode: Qt.Key_J;       firstSymbol: "?"; symbolKeycode: Qt.Key_Question ;     accent: "í"; accentKeycode: Qt.Key_Iacute  }
        ListElement { letter: "k"; keycode: Qt.Key_K;       firstSymbol: "~"; symbolKeycode: Qt.Key_AsciiTilde;    accent: "ï"; accentKeycode: Qt.Key_Idiaeresis  }
        ListElement { letter: "l"; keycode: Qt.Key_L;       firstSymbol: "|"; symbolKeycode: Qt.Key_Bar ;          accent: "ñ"; accentKeycode: Qt.Key_Ntilde  }
        ListElement { letter: "m"; keycode: Qt.Key_M;       firstSymbol: "`"; symbolKeycode: Qt.Key_QuoteLeft;     accent: "µ"; accentKeycode: Qt.Key_mu  }
	ListElement { letter: "-"; keycode: Qt.Key_Minus;   firstSymbol: "'"; symbolKeycode: Qt.Key_Apostrophe;    accent: "£"; accentKeycode: Qt.Key_sterling }
    }
    ListModel {
        id:third
        ListElement { letter: "w"; keycode: Qt.Key_W;       firstSymbol: "_";  symbolKeycode: Qt.Key_Underscore;   accent: "ò"; accentKeycode: Qt.Key_Ograve }
        ListElement { letter: "x"; keycode: Qt.Key_X;       firstSymbol: "(";  symbolKeycode: Qt.Key_ParenLeft;    accent: "ô"; accentKeycode: Qt.Key_Ocircumflex  }
        ListElement { letter: "c"; keycode: Qt.Key_C;       firstSymbol: ")";  symbolKeycode: Qt.Key_ParenRight;   accent: "ó"; accentKeycode: Qt.Key_Oacute  }
        ListElement { letter: "v"; keycode: Qt.Key_V;       firstSymbol: "[";  symbolKeycode: Qt.Key_BracketLeft;  accent: "ö"; accentKeycode: Qt.Key_Odiaeresis  }
        ListElement { letter: "b"; keycode: Qt.Key_B;       firstSymbol: "]";  symbolKeycode: Qt.Key_BracketRight; accent: "õ"; accentKeycode: Qt.Key_Otilde  }
        ListElement { letter: "n"; keycode: Qt.Key_N;       firstSymbol: "{";  symbolKeycode: Qt.Key_BraceLeft;    accent: "ù"; accentKeycode: Qt.Key_Ugrave  }
        ListElement { letter: "@"; keycode: Qt.Key_At;      firstSymbol: "}";  symbolKeycode: Qt.Key_BraceRight;   accent: "û"; accentKeycode: Qt.Key_Ucircumflex  }
        ListElement { letter: "."; keycode: Qt.Key_Comma;   firstSymbol: ",";  symbolKeycode: Qt.Key_Colon;        accent: "ú"; accentKeycode: Qt.Key_Uacute  }
        ListElement { letter: ":"; keycode: Qt.Key_Period;  firstSymbol: ";";  symbolKeycode: Qt.Key_Semicolon;    accent: "ü"; accentKeycode: Qt.Key_Udiaeresis  }
        ListElement { letter: "/"; keycode: Qt.Key_Slash;   firstSymbol: "\\"; symbolKeycode: Qt.Key_Backslash;    accent: "<"; accentKeycode: Qt.Key_Less  }
	ListElement { letter: "*"; keycode: Qt.Key_Asterisk;firstSymbol: "\""; symbolKeycode: Qt.Key_QuoteDbl;     accent: ">"; accentKeycode: Qt.Key_Greater  }
    }
}
